/**
 * @typedef  {("ACTIVE" | "ADMIN" | "EMAIL_VERIFIED" | "PAYMENT_VERIFIED" | "TFA_ENABLED")}  possibleStatus
 */

class User{
  #statusEnum = {
    // NO_STATUS       : 0,
    ACTIVE          : 1,    // 2^0
    ADMIN           : 2,    // 2^1
    EMAIL_VERIFIED  : 4,    // 2^2
    PAYMENT_VERIFIED: 8,    // 2^3
    TFA_ENABLED     : 16,   // 2^4
  }

  email;
  name;
  status = 0;

  /**
   * [construct description]
   *
   * @param   {Object}  props
   * @param   {String}  props.email  [email description]
   * @param   {String}  props.name   [name description]
   *
   * @constructor
   */
  constructor({email, name}){
    this.email = email;
    this.name  = name;
  }

  /**
   * update current user's status
   *
   * @param   {possibleStatus} newStatus
   *
   * @return  {void}        [return description]
   */
  updateStatus(newStatus){
    this.status= (this.status ^= this.#statusEnum[newStatus]);
  }

  /**
   * [hasStatus description]
   * @param   {possibleStatus} status
   *
   * @return  {Boolean}  [return description]
   */
  hasStatus(status){
    return ( this.status & this.#statusEnum[status]) === this.#statusEnum[status];
  }

  /**
   * log all status of the user
   *
   * @return  {void}  give information in console
   */
  getStatus(){
    if (this.status === 0) return console.log("NO_STATUS");
    for (const statusName of Object.keys(this.#statusEnum)) {
      if (!this.hasStatus(statusName)) continue;
      console.log(statusName);
    }
  }
}

console.clear()
const elon = new User({"name":"Elon Musk", "email":"elon@tesla.com"});
elon.updateStatus("TFA_ENABLED");
elon.updateStatus("ACTIVE");
console.log(elon);
elon.getStatus();
